#!/bin/bash
myVar=`cat /var/lib/jenkins/workspace/wireguard/wireguardClientSetup/client_private_key.txt`
echo "[Interface]
Address = 10.100.100.2/32
PrivateKey = $myVar

[Peer]
PublicKey = 5q5uEtOY1H4Dh1hTwE7R/WwMoqrWn880kfT4LB0v+30=
Endpoint = 3.86.205.142:51820
AllowedIPs = 0.0.0.0/0
PersistentKeepalive = 21" > wg0-client.conf
